1. Membuat Database 
>>  create database myshop;

2. Membuat Table
>> create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
>> create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(12),
    -> stock int(10),
    -> category_id int(10),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );
>>  create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. Memasukan Data
>> insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
>> insert into items(name, description, price, stock, category_id) values("Samsung b50", "hape keren dari merek sumsang", "4000000", "100", 1),("Uniklooh", "baju keren dari brand ternama", "500000", "50", 2),("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1);
>>  insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

4. Mengambil Data
   a. select id, name, email from users;
   b. select * from items where price > 1000000;
      select * from items where name like 'uniklo%';
   c. select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories;

5. update items set price = "2500000" where id = 1;